package com.test.volatiletest;

import java.util.concurrent.TimeUnit;

public class VolatileMain {

    public static volatile boolean sTag = true;

    public static void main(String[] args) throws InterruptedException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (sTag) {

                }
                System.out.println("thread finish");
            }
        }).start();

        TimeUnit.SECONDS.sleep(1);

        System.out.println("main finish");
        VolatileMain.sTag = false;
    }
}
