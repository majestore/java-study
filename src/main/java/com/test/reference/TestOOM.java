package com.test.reference;

import java.lang.ref.SoftReference;

public class TestOOM {

    public static void main(String[] args) {
        ReferenceDemo referenceDemo = new ReferenceDemo();

//        referenceDemo.testStrongReference(3);
//        referenceDemo.testSoftReference(1);
        referenceDemo.testSoftReferenceLoop(1);
//        referenceDemo.testWeakReferenceLoop(1);
    }
}
