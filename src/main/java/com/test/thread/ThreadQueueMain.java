package com.test.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadQueueMain {

    public static void main(String[] args) throws IOException {
        LinkedBlockingQueue<SimpleThread> linkedBlockingQueue = new LinkedBlockingQueue<>();

        Thread whileThread = new Thread(() -> {
            while (true) {
                if (linkedBlockingQueue.size() > 0) {
                    linkedBlockingQueue.poll().start();
                }
            }
        });
        whileThread.start();

        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String inputString = null;
            System.out.println("please enter a value:");
            inputString = br.readLine();

//            System.out.println("value is: " + str);
            linkedBlockingQueue.offer(new SimpleThread(inputString));
        }
    }
}
