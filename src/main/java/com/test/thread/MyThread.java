package com.test.thread;

public class MyThread implements Runnable {
    private final String threadName;
    private final Long duration;

    public MyThread(String threadName, Long duration) {
        this.threadName = threadName;
        this.duration = duration;
    }

    @Override
    public void run() {
        try {
            System.out.println("MyThread start: " + this.threadName);
            Thread.sleep(this.duration);
            System.out.println("MyThread finish: " + this.threadName);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
