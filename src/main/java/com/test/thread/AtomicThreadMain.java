package com.test.thread;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class AtomicThreadMain {

    public static void main(String[] args) throws IOException {
        AtomicInteger atomicInteger = new AtomicInteger();
        ReentrantLock reentrantLock = new ReentrantLock(false);

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                reentrantLock.lock();
                int result = atomicInteger.getAndIncrement();
                String threadName = Thread.currentThread().getName();
                for (int j = 0; j < 10; j++) {
                    System.out.printf("threadName %s result: %d index: %d\n", threadName, result, j);
                }
                reentrantLock.unlock();
            }).start();
        }

//        System.out.println("value is: " + atomicInteger.get());
    }
}
