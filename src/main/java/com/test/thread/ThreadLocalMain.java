package com.test.thread;

public class ThreadLocalMain {

    public static void main(String[] args) {
        String threadName = Thread.currentThread().getName();
        ThreadLocal<String> threadLocal1 = new ThreadLocal<>();
        threadLocal1.set("hello");
        System.out.println(threadName + ": " + threadLocal1.get());

        ThreadLocal<String> threadLocal2 = new ThreadLocal<>();
        threadLocal2.set("world");
        System.out.println(threadName + ": " + threadLocal2.get());

        new Thread(() -> {
            String localThreadName = Thread.currentThread().getName();
            System.out.println(localThreadName + ": " + threadLocal1.get());
        }).start();

        new Thread(() -> {
            String localThreadName = Thread.currentThread().getName();
            System.out.println(localThreadName + ": " + threadLocal2.get());
        }).start();
    }
}
