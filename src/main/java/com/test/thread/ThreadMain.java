package com.test.thread;

import java.util.concurrent.TimeUnit;

public class ThreadMain {

    static void syncRun(long duration, int count, String threadName) {
        try {
            System.out.printf("thread start name: %s\n", threadName);
            for (int i = 0; i < count; i++) {
                Thread.sleep(duration);
                System.out.printf("thread start name: %s %d\n", threadName, i);
            }
            System.out.printf("thread finish name: %s\n", threadName);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static void syncRun2(String threadName) {
        while (true) {
            boolean isInterrupt = Thread.currentThread().isInterrupted();
            if (isInterrupt) {
                System.out.printf("thread name: %s is interrupted\n", threadName);
                break;
            }
            System.out.printf("thread name: %s %b\n", threadName, isInterrupt);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        /*SuperThread firstThread = new SuperThread("first thread", 10);
        firstThread.start();
        SuperThread secondThread = new SuperThread("second thread", 10, () -> {
            System.out.println("secondThread running");
        });
        secondThread.start();*/

        /*MyThread myThread1 = new MyThread("first thread", 3000L);
        myThread1.run();
        MyThread myThread2 = new MyThread("second thread", 3000L);
        myThread2.run();*/

        Thread helloThread = new Thread(() -> {
//            ThreadMain.syncRun(1000, 10, "helloThread");
            ThreadMain.syncRun2("helloThread");
        });
        helloThread.start();

        TimeUnit.SECONDS.sleep(2);
        helloThread.interrupt();
        helloThread.join();

        /*Thread worldThread = new Thread(() -> {
            ThreadMain.syncRun(1000, 5, "worldThread");
        });
        worldThread.start();*/

        System.out.println("main thread finish");
    }
}
