package com.test.thread;

public class SimpleThread extends Thread {
    private final String threadName;

    public SimpleThread(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10000);
            System.out.println("SimpleThread: " + this.threadName);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
