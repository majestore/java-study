package com.test.thread;

import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockMain {

    public static void main(String[] args) throws IOException {
        ReentrantLock reentrantLock = new ReentrantLock(false);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                reentrantLock.lock();
                String threadName = Thread.currentThread().getName();
                for (int j = 0; j < 10; j++) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("threadName: " + threadName + " index: " + j);
                }
                reentrantLock.unlock();
            }).start();
        }

        /*while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("please enter a value:");
            String inputString = br.readLine();
            System.out.println("value is: " + inputString);
        }*/
    }
}
