package com.test.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.locks.ReentrantLock;

public class SyncThreadMain {

    private void workProcess(int count) throws InterruptedException {
        for (int i = 0; i < count; i++) {
            Thread.sleep(1000);
            String threadName = Thread.currentThread().getName();
            System.out.println("workProcess: " + threadName + " i: " + i);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        Object lock = new Object();
        SyncThreadMain syncThreadMain = new SyncThreadMain();
        /*for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    syncThreadMain.workProcess();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }*/

        Thread thread = new Thread(() -> {
            try {
                synchronized (lock) {
                    syncThreadMain.workProcess(10);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();

        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("please enter a value:");
            String inputString = br.readLine();
            if (inputString.equals("stop")) {
                lock.wait();
            } else if (inputString.equals("start")) {
                lock.notify();
            }
        }
    }
}
