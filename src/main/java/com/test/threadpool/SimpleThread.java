package com.test.threadpool;

public class SimpleThread extends Thread {
    @Override
    public void run() {
        super.run();

        Long threadId = getId();
        String threadName = getName();
//        System.out.printf("thread start threadId: %d threadName: %s\n", threadId, threadName);
        try {
            for (int i = 0; i < 10; i++) {
                Thread.sleep(200L);
                System.out.printf("thread loop threadId: %d threadName: %s index: %d\n", threadId, threadName, i);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        System.out.printf("thread finish threadId: %d threadName: %s\n", threadId, threadName);
    }
}
