package com.test.threadpool;

public interface BaseAdapter {
    void createThreadPool();
    void executeThreadPool();
}
