package com.test.threadpool;

import com.test.thread.SimpleThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.*;

public class ThreadPoolMain {

    public static void main(String[] args) throws InterruptedException, IOException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2, 4, 1000L, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(5),
                Executors.defaultThreadFactory(),
                new ThreadPoolException());

        /*for (int i = 0; i < 10; i++) {
            threadPoolExecutor.execute(new MaxThread(10 * 1000L));
        }*/

        /*ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executorService.execute(new SimpleThread());
        }*/

        /*ExecutorService executorService = Executors.newFixedThreadPool(1);
        for (int i = 0; i < 5; i++) {
            executorService.execute(new SimpleThread());
        }*/

//        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(3);
//        scheduledExecutorService.scheduleAtFixedRate(new SimpleThread(), 3, 5, TimeUnit.SECONDS);
//        scheduledExecutorService.scheduleWithFixedDelay(new SimpleThread(), 3, 5, TimeUnit.SECONDS);

//        scheduledExecutorService.shutdown();

        MaxThread maxThread = new MaxThread(2 * 1000L);
        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String inputString = null;
            System.out.println("please enter a value:");
            inputString = br.readLine();
            System.out.println("value is: " + inputString);
            if (inputString.equals("quit")) {
                return;
            }

            threadPoolExecutor.execute(maxThread);
        }
    }
}
