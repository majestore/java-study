package com.test.threadpool;

public class MaxThread extends Thread {
    private final Long duration;

    public MaxThread(Long duration) {
        this.duration = duration;
    }

    @Override
    public void run() {
        try {
            long threadId = getId();
            String threadName = getName();
            System.out.printf("thread start id: %d name: %s\n", threadId, threadName);
            sleep(this.duration);
            System.out.printf("thread finish id: %d name: %s\n", threadId, threadName);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
