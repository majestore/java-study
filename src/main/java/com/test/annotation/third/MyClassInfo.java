package com.test.annotation.third;

@MyInfo(id = 999, name = "superInfo")
public class MyClassInfo {

    @MyInfo(id = 880, name = "trans")
    private int id;

    @MyInfo(id = 881, name = "white")
    public String name;

    public MyClassInfo() {
    }

    @MyInfo(id = 777, name = "black")
    public MyClassInfo(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @MyInfo(id = 666, name = "red")
    private int getId(int id) {
        return id * 100;
    }

    @MyInfo(id = 555, name = "blue")
    public String getName(String name) {
        return "color: " + name;
    }
}
