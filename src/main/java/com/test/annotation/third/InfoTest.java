package com.test.annotation.third;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InfoTest {

    public static void main(String[] args) {
        Class<?> myClassInfoClass = MyClassInfo.class;
        if (!myClassInfoClass.isAnnotationPresent(MyInfo.class)) {
            System.out.println("no annotation");
            return;
        }

        MyInfo myInfoClassAnnotation = myClassInfoClass.getAnnotation(MyInfo.class);
        System.out.printf("%d %s\n", myInfoClassAnnotation.id(), myInfoClassAnnotation.name());

        try {
            //无参构造函数实例
//            Object objectInstance = myClassInfoClass.newInstance();

            //有参构造函数实例
            Constructor<?> myClassInfoClassConstructor = myClassInfoClass.getConstructor(int.class, String.class);
            Object objectInstance = myClassInfoClassConstructor.newInstance(101, "unknown");
            if (myClassInfoClassConstructor.isAnnotationPresent(MyInfo.class)) {
                MyInfo constructorAnnotation = myClassInfoClassConstructor.getAnnotation(MyInfo.class);
                System.out.printf("%d %s\n", constructorAnnotation.id(), constructorAnnotation.name());
            }

            //私有成员变量
            Field infoId = myClassInfoClass.getDeclaredField("id");
            MyInfo infoIdAnnotation = infoId.getAnnotation(MyInfo.class);
            System.out.printf("%d %s\n", infoIdAnnotation.id(), infoIdAnnotation.name());

            //公有成员变量
            Field infoName = myClassInfoClass.getField("name");
            MyInfo infoNameAnnotation = infoName.getAnnotation(MyInfo.class);
            System.out.printf("%d %s\n", infoNameAnnotation.id(), infoNameAnnotation.name());

            //私有方法
            Method getIdMethod = myClassInfoClass.getDeclaredMethod("getId", int.class);
            if (getIdMethod.isAnnotationPresent(MyInfo.class)) {
                MyInfo getIdMethodAnnotation = getIdMethod.getAnnotation(MyInfo.class);
                System.out.printf("%d %s\n", getIdMethodAnnotation.id(), getIdMethodAnnotation.name());

                getIdMethod.setAccessible(true);
                int invokeId = (int) getIdMethod.invoke(objectInstance, getIdMethodAnnotation.id());
                System.out.println(invokeId);
            }

            //共有方法
            Method getNameMethod = myClassInfoClass.getMethod("getName", String.class);
            if (getNameMethod.isAnnotationPresent(MyInfo.class)) {
                MyInfo getNameMethodAnnotation = getNameMethod.getAnnotation(MyInfo.class);
                System.out.printf("%d %s\n", getNameMethodAnnotation.id(), getNameMethodAnnotation.name());

                String invokeName = (String) getNameMethod.invoke(objectInstance, getNameMethodAnnotation.name());
                System.out.println(invokeName);
            }
        } catch (NoSuchFieldException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
