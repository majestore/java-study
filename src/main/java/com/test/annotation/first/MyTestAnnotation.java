package com.test.annotation.first;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
public @interface MyTestAnnotation {
    String username() default "wang";
    int age() default 35;
}
