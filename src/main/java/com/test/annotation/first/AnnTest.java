package com.test.annotation.first;

public class AnnTest {

    public static void main(String[] args) {
        Class<Parent> parentClass = Parent.class;
        boolean parentClassAnnotationPresent = parentClass.isAnnotationPresent(MyTestAnnotation.class);
        if (parentClassAnnotationPresent) {
            MyTestAnnotation parentClassAnnotation = parentClass.getAnnotation(MyTestAnnotation.class);
            System.out.println(parentClassAnnotation.username());
            System.out.println(parentClassAnnotation.age());
        }

        Class<Children> childrenClass = Children.class;
        boolean childrenClassAnnotationPresent = childrenClass.isAnnotationPresent(MyTestAnnotation.class);
        if (childrenClassAnnotationPresent) {
            MyTestAnnotation childrenClassAnnotation = childrenClass.getAnnotation(MyTestAnnotation.class);
            System.out.println(childrenClassAnnotation.username());
            System.out.println(childrenClassAnnotation.age());
        }
    }
}
