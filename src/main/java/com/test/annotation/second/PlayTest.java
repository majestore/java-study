package com.test.annotation.second;

public class PlayTest {

    public static void main(String[] args) {
        Class<PlayGame> playGameClass = PlayGame.class;
        boolean annotationPresent = playGameClass.isAnnotationPresent(Player.class);
        if (annotationPresent) {
            Player playGameClassAnnotation = playGameClass.getAnnotation(Player.class);
            Game[] games = playGameClassAnnotation.value();
            for (Game game : games) {
                System.out.println(game.value());
            }
        }
    }
}
