package com.test.annotation.second;

import java.lang.annotation.*;

@Documented
@Repeatable(Player.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Game {
    String value() default "";
}
