package com.test.proxy_object;

public interface MyBusiness {

    void method1();

    void method2();
}
