package com.test.proxy_object;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyObjectTest {

    public static void main(String[] args) {
        MyBusiness myBusiness = new MyBusinessImpl();
        MyBusiness proxyInstance = (MyBusiness) Proxy.newProxyInstance(ProxyObjectTest.class.getClassLoader(), new Class[]{MyBusiness.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.getName().equals("method1")) {
                    System.out.println("********重写了method1***********");
                    return null;
                } else {
                    return method.invoke(myBusiness, args);
                }

            }
        });
        proxyInstance.method1();
        proxyInstance.method2();
    }
}
