package com.test.base;

public class BaseTest {

    static class Singleton {
        private static Singleton singleton = null;

        private Singleton() {

        }

        public static Singleton getInstance() {
            if (singleton == null) {
                singleton = new Singleton();
            }

            return singleton;
        }

        public void showInfo() {
            System.out.println("Singleton");
        }
    }

    public static void main(String[] args) {
        System.out.println("hello world");

        Singleton singleton = Singleton.getInstance();
        singleton.showInfo();

        BaseSingleton baseSingleton = BaseSingleton.getInstance();
        baseSingleton.showInfo();
    }
}
