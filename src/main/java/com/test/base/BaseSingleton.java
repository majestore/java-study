package com.test.base;

public class BaseSingleton {
    private static BaseSingleton baseSingleton = null;

    private BaseSingleton() {

    }

    public static BaseSingleton getInstance() {
        if (baseSingleton == null) {
            baseSingleton = new BaseSingleton();
        }

        return  baseSingleton;
    }

    public void showInfo() {
        System.out.println("BaseSingleton");
    }
}
