package com.test.queue;

import java.util.concurrent.LinkedBlockingQueue;

public class QueueMain {

    static class MyThread extends Thread {
        private final int count;

        public MyThread(int count) {
            this.count = count;
        }

        @Override
        public void run() {
            super.run();

            try {
                for (int i = 0; i < this.count; i++) {
                    sleep(1000);
                    System.out.printf("threadName: %s\n", getName());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        LinkedBlockingQueue<MyThread> linkedBlockingQueue = new LinkedBlockingQueue<>();
        for (int i = 0; i < 2; i++) {
            linkedBlockingQueue.offer(new MyThread(3));
        }

        while (linkedBlockingQueue.size() > 0) {
            linkedBlockingQueue.poll().start();
        }
    }
}
