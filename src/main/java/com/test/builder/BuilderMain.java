package com.test.builder;

public class BuilderMain {
    public static void main(String[] args) {
//        BuildUser buildUser = new BuildUser();
        BuildUser.InternalUser user = new BuildUser().build().setUsername("peter")
                .setPassword("111111")
                .setGender(1)
                .setAge(22)
                .setHeight(166.5)
                .setWeight(53.8)
                .create();

        System.out.println(user.toString());
    }
}
