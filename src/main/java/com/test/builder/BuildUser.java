package com.test.builder;

public class BuildUser {
    public static class InternalUser {
        private String username;
        private String password;
        private int gender;
        private int age;
        private double height;
        private double weight;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public double getHeight() {
            return height;
        }

        public void setHeight(double height) {
            this.height = height;
        }

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }

        @Override
        public String toString() {
            return "User{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    ", gender=" + gender +
                    ", age=" + age +
                    ", height=" + height +
                    ", weight=" + weight +
                    '}';
        }
    }

    private InternalUser user;

    public BuildUser build() {
        this.user = new InternalUser();
        return this;
    }

    public BuildUser setUsername(String username) {
        this.user.setUsername(username);
        return this;
    }

    public BuildUser setPassword(String password) {
        this.user.setPassword(password);
        return this;
    }

    public BuildUser setGender(int gender) {
        this.user.setGender(gender);
        return this;
    }

    public BuildUser setAge(int age) {
        this.user.setAge(age);
        return this;
    }

    public BuildUser setHeight(double height) {
        this.user.setHeight(height);
        return this;
    }

    public BuildUser setWeight(double weight) {
        this.user.setWeight(weight);
        return this;
    }

    public InternalUser create() {
        return user;
    }
}
