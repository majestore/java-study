package com.test.hashcode;

import java.util.Date;

public class HashCodeMain {

    static int hash(int h) {
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    public static void main(String[] args) {
        Date date = new Date();
        int hashCodeA = date.hashCode();
        System.out.println("hashCodeA: " + hashCodeA);

        int hashCode = hash(hashCodeA);
        System.out.println("hashCode: " + hashCode);
    }
}
