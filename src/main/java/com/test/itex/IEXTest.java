package com.test.itex;

public class IEXTest {

    public static void main(String[] args) {
        MyTest myTest = new MyTest();

        FuncTest funcTest = new FuncTest();
        funcTest.createInterfaceTest(myTest);
        funcTest.createExtendTest(myTest);
    }
}
