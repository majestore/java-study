package com.test.jwt;

import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class JwtTest {

    private static final String SIGN_KEY = "1qazxsw2";

    public static String getMD5Str(String str) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("md5");
        byte[] digest  = md5.digest(str.getBytes(StandardCharsets.UTF_8));
        //16是表示转换为16进制数
        String md5Str = new BigInteger(1, digest).toString(16);
        return md5Str;
    }

    public static void main(String[] args) {
        try {
            JSONObject header = new JSONObject();
            header.put("alg", "HS256");

            JSONObject payload = new JSONObject();
            payload.put("phone", "13344455667");

            String headerJsonStr = header.toJSONString();
            String headerJwt = Base64.getEncoder().encodeToString(headerJsonStr.getBytes());
            String payloadJsonStr = payload.toJSONString();
            String payloadJwt = Base64.getEncoder().encodeToString(payloadJsonStr.getBytes());

            String signStr = JwtTest.getMD5Str(payloadJsonStr + SIGN_KEY);
            String jwtStr = headerJwt + "." + payloadJwt + "." + signStr;
            System.out.println(jwtStr);

            String[] jwtSplit = jwtStr.split("\\.");
            String payloadDecoder = new String(Base64.getDecoder().decode(jwtSplit[1]), StandardCharsets.UTF_8);
            String md5DecoderStr = JwtTest.getMD5Str(payloadDecoder + SIGN_KEY);
            System.out.println(md5DecoderStr);
            System.out.println(jwtSplit[2]);
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }
    }
}
