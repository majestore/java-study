package com.test.factory;

public class DellFactory implements FactoryProvider {
    @Override
    public Computer product() {
        return new DellComputer();
    }
}
