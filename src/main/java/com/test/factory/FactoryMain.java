package com.test.factory;

public class FactoryMain {
    public static void main(String[] args) {
        Computer dellComputer = BaseFactory.produceDellComputer();
        Computer lenovoComputer = BaseFactory.produceLenovoComputer();

        dellComputer.build();
        lenovoComputer.build();

        FactoryProvider dellFactory = new DellFactory();
        dellFactory.product().build();

        FactoryProvider lenovoFactory = new LenovoFactory();
        lenovoFactory.product().build();
    }
}
