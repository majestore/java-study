package com.test.factory;

public interface Computer {
    void build();
}
