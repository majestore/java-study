package com.test.factory;

public class BaseFactory {
    public static Computer produceDellComputer() {
        return new DellComputer();
    }

    public static Computer produceLenovoComputer() {
        return new LenovoComputer();
    }
}
