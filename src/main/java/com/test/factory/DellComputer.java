package com.test.factory;

public class DellComputer implements Computer {
    @Override
    public void build() {
        System.out.println("DellComputer build");
    }
}
