package com.test.factory;

public class LenovoFactory implements FactoryProvider {
    @Override
    public Computer product() {
        return new LenovoComputer();
    }
}
