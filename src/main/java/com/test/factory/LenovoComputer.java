package com.test.factory;

public class LenovoComputer implements Computer {
    @Override
    public void build() {
        System.out.println("LenovoComputer build");
    }
}
