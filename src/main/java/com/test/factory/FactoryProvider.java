package com.test.factory;

public interface FactoryProvider {
    Computer product();
}
