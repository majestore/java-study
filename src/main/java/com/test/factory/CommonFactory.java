package com.test.factory;

public class CommonFactory<T> implements FactoryProvider {
    @Override
    public Computer product() {
        return new DellComputer();
    }
}
