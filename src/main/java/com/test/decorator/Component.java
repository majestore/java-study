package com.test.decorator;

public interface Component {
    void operation();
}
