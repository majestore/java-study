package com.test.decorator;

public class DecoratorTest {

    public static void main(String[] args) {
        Component dctComponent = new ConcreteComponent();
        dctComponent.operation();

        Component cctDecorator = new ConcreteDecorator(dctComponent);
        cctDecorator.operation();;
    }
}
