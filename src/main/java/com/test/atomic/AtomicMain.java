package com.test.atomic;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicMain {

    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        System.out.println(atomicInteger.get());

        atomicInteger.set(11);
        System.out.println(atomicInteger.get());
        System.out.println(atomicInteger.getAndAdd(12));
    }
}
