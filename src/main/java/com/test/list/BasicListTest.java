package com.test.list;

import java.util.ArrayList;
import java.util.List;

public class BasicListTest {
    public static void main(String[] args) {
        List<Integer> basicList = new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(3);
        }};
        System.out.println(basicList.size());
        List<Integer> basicList2 = new ArrayList<Integer>(2) {{
            add(11);
            add(12);
            add(13);
        }};
        System.out.println(basicList2.size());
    }
}
