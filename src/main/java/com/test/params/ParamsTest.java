package com.test.params;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParamsTest {

    private void testParams(String ...params) {
        for (String param : params) {
            System.out.println(param);
        }
    }

    public static void main(String[] args) {
        ParamsTest paramsTest = new ParamsTest();
        paramsTest.testParams("a", "b", "c");

        String[] strArray = new String[]{"hello", "world"};
        paramsTest.testParams(strArray);


        List<String> strList = new ArrayList<String>(){{
            add("abc");
            add("bcd");
            add("def");
        }};

        String[] newStrArray = new String[strList.size()];
        strList.toArray(newStrArray);
        for (String abc : newStrArray) {
            System.out.println(abc);
        }
    }
}
