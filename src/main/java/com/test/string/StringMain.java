package com.test.string;

public class StringMain {

    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("hello").append(1).append("world");
        System.out.println(stringBuilder.toString());

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(1).append(2).append(3);
        System.out.println(stringBuffer.toString());
    }
}
