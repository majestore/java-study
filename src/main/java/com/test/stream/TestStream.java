package com.test.stream;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestStream {

    public void test1() {
        String[] testStrings = { "java", "react", "angular", "vue" };
        Stream<String> stringStream = Stream.of(testStrings);
        List<String> list = stringStream.collect(Collectors.toList());
        list.forEach(System.out::println);
    }

    public void test2() {
        List<Integer> list = new ArrayList<>(Arrays.asList(3,5,2,9,1,6,8,7));
        list.stream().filter(integer -> integer > 5).sorted().forEach(System.out::println);

        IntStream.range(1, 5).forEach(System.out::println);
    }

    /**
     * 生成流
     */
    public void test3() {
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");

        List<String> collect1 = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println(collect1);

        Stream<List<String>> listStream = Stream.of(strings);
        List<List<String>> collect2 = listStream.filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println(collect2);
    }

    /**
     * limit
     */
    public void test4() {
        Random random = new Random();
        random.ints().limit(10).forEach(System.out::println);
    }

    /**
     * map
     */
    public void test5() {
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        List<Integer> squaresList = numbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
        System.out.println(squaresList);

        StreamUser streamUser1 = new StreamUser() {{
            setUserId(1L);
            setUsername("alex");
            setPassword("111111");
            setAge(15);
            setGender("male");
        }};
        StreamUser streamUser2 = new StreamUser() {{
            setUserId(1L);
            setUsername("brown");
            setPassword("111111");
            setAge(12);
            setGender("male");
        }};
        Stream<StreamUser> streamUserStream = Stream.of(streamUser1, streamUser2);
        streamUserStream.sorted(Comparator.comparingInt(StreamUser::getAge)).map(StreamUser::getUsername).forEach(System.out::println);
    }

    /**
     * filter
     */
    public void test6() {
        List<String>strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        long count = strings.stream().filter(string -> !string.isEmpty()).count();
        System.out.println(count);
    }

    /**
     * sorted
     */
    public void test7() {
        StreamUser streamUser1 = new StreamUser();
        streamUser1.setUserId(1L);
        streamUser1.setUsername("alex");
        streamUser1.setPassword("111111");
        streamUser1.setAge(15);
        streamUser1.setGender("male");

        StreamUser streamUser2 = new StreamUser();
        streamUser2.setUserId(2L);
        streamUser2.setUsername("brown");
        streamUser2.setPassword("111111");
        streamUser2.setAge(12);
        streamUser2.setGender("male");

        StreamUser streamUser3 = new StreamUser();
        streamUser3.setUserId(3L);
        streamUser3.setUsername("chris");
        streamUser3.setPassword("111111");
        streamUser3.setAge(11);
        streamUser3.setGender("male");

        StreamUser streamUser4 = new StreamUser();
        streamUser4.setUserId(4L);
        streamUser4.setUsername("dennis");
        streamUser4.setPassword("111111");
        streamUser4.setAge(14);
        streamUser4.setGender("male");

        Stream<StreamUser> streamUserStream = Stream.of(streamUser1, streamUser2, streamUser3, streamUser4);
        streamUserStream.sorted(Comparator.comparing(StreamUser::getUsername, Comparator.reverseOrder())).forEach(System.out::println);
//        streamUserStream.sorted((x, y) -> Integer.parseInt(x.getUsername())).forEach(item -> System.out.println(item.getAge()));
    }

    /**
     * parallel
     */
    public void test8() {
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        long count = strings.parallelStream().filter(String::isEmpty).count();
        System.out.println(count);
    }

    /**
     * to map
     */
    public void test9() {
        List<StreamUser> userList = new ArrayList<>();
        StreamUser streamUser1 = new StreamUser();
        streamUser1.setUserId(1L);
        streamUser1.setUsername("alex");
        streamUser1.setPassword("111111");
        streamUser1.setAge(15);
        streamUser1.setGender("male");
        userList.add(streamUser1);

        StreamUser streamUser2 = new StreamUser();
        streamUser2.setUserId(2L);
        streamUser2.setUsername("brown");
        streamUser2.setPassword("111111");
        streamUser2.setAge(12);
        streamUser2.setGender("male");
        userList.add(streamUser2);

        StreamUser streamUser3 = new StreamUser();
        streamUser3.setUserId(3L);
        streamUser3.setUsername("chris");
        streamUser3.setPassword("111111");
        streamUser3.setAge(11);
        streamUser3.setGender("male");
        userList.add(streamUser3);

        StreamUser streamUser4 = new StreamUser();
        streamUser4.setUserId(4L);
        streamUser4.setUsername("dennis");
        streamUser4.setPassword("111111");
        streamUser4.setAge(14);
        streamUser4.setGender("male");
        userList.add(streamUser4);

        Map<Long, StreamUser> collect1 = userList.stream().collect(Collectors.toMap(StreamUser::getUserId, StreamUser -> StreamUser));
        System.out.println(collect1);

        Map<Long, StreamUser> collect2 = userList.stream().collect(Collectors.toMap(item -> item.getUserId(), item -> item));
        System.out.println(collect2);

        Map<Long, StreamUser> collect3 = userList.stream().collect(Collectors.toMap(StreamUser::getUserId, Function.identity()));
        System.out.println(collect3);
    }

    public static void main(String[] args) {
        TestStream testStream = new TestStream();
        testStream.test1();
        testStream.test2();
        testStream.test3();
        testStream.test4();
        testStream.test5();
        testStream.test6();
        testStream.test7();
        testStream.test8();
        testStream.test9();
    }
}
