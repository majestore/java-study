package com.test.juc;

import com.test.juc.resource.Ticket;

public class ReentrantLockTest {

    public static void main(String[] args) {
        Ticket ticket = new Ticket(true);

        String[] dataList = new String[]{"A", "B", "C"};
        for (String dataItem : dataList) {
            Thread threadA = new Thread(() -> {
                for (int i = 0; i < 40; i++) {
                    ticket.lockSaleTicket();
                }
            }, dataItem);
            threadA.start();
        }

        /*Thread threadA = new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.lockSaleTicket();
            }
        }, "A");
        threadA.start();

        Thread threadB = new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.lockSaleTicket();
            }
        }, "B");
        threadB.start();

        Thread threadC = new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.lockSaleTicket();
            }
        }, "C");
        threadC.start();*/
    }
}
