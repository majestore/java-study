package com.test.juc;

import com.test.juc.resource.Ticket;

public class SynchronizedTest {

    public static void main(String[] args) {
        Ticket ticket = new Ticket();

        Thread threadA = new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.syncSaleTicket();
            }
        }, "A");
        threadA.start();

        Thread threadB = new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.syncSaleTicket();
            }
        }, "B");
        threadB.start();

        Thread threadC = new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.syncSaleTicket();
            }
        }, "C");
        threadC.start();
    }
}
