package com.test.juc;

import com.test.juc.resource.ShareResource;

public class ThreadReentrantLockTest2 {

    public static void main(String[] args) {
        ShareResource shareResource = new ShareResource();

        Thread threadTA = new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    shareResource.print5(i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "TA");
        threadTA.start();

        Thread threadTB = new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    shareResource.print10(i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "TB");
        threadTB.start();

        Thread threadTC = new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    shareResource.print15(i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "TC");
        threadTC.start();
    }
}
