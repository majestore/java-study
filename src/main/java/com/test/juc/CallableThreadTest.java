package com.test.juc;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

class CallThread implements Callable<Integer> {

    private int initData;

    public CallThread(int initData) {
        this.initData = initData;
    }

    public CallThread setInitData(int initData) {
        this.initData = initData;
        return this;
    }

    @Override
    public Integer call() throws Exception {
        TimeUnit.SECONDS.sleep(1);
        System.out.println("CallThread");
        return initData * 100;
    }
}

public class CallableThreadTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CallThread callThread = new CallThread(1);
        FutureTask<Integer> futureTask = new FutureTask<>(callThread);

        Thread thread = new Thread(futureTask);
        thread.start();

        /*while (!futureTask.isDone()) {
            System.out.println("futureTask not done");
        }*/

        Integer taskInteger = futureTask.get();
        System.out.println(taskInteger);

        FutureTask<Integer> futureTask2 = new FutureTask<>(callThread.setInitData(2));
        new Thread(futureTask2).start();

        Integer taskInteger2 = futureTask2.get();
        System.out.println(taskInteger2);

        System.out.println(Thread.currentThread().getName() + " is done");
    }
}
