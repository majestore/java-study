package com.test.juc;

import java.util.concurrent.TimeUnit;

public class DeadLockTest {

    static final Object a = new Object();
    static final Object b = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread threadA = new Thread(() -> {
            synchronized (a) {
                System.out.printf("%s get lock a, will get b.\n", Thread.currentThread().getName());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (b) {
                    System.out.printf("%s get lock b.\n", Thread.currentThread().getName());
                }
            }
        });
        threadA.start();

//        TimeUnit.SECONDS.sleep(5);

        Thread threadB = new Thread(() -> {
            synchronized (b) {
                System.out.printf("%s get lock b, will get a.\n", Thread.currentThread().getName());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (a) {
                    System.out.printf("%s get lock a.\n", Thread.currentThread().getName());
                }
            }
        });
        threadB.start();

        threadA.join();
        threadB.join();

        System.out.printf("%s quit\n", Thread.currentThread().getName());
    }
}
