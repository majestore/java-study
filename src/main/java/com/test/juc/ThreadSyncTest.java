package com.test.juc;

import com.test.juc.resource.Share;

public class ThreadSyncTest {

    public static void main(String[] args) {
        Share share = new Share();

        Thread threadTA = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    share.incr();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "TA");
        threadTA.start();

        Thread threadTB = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    share.decr();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "TB");
        threadTB.start();
    }
}
