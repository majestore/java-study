package com.test.reflect;

public class User {

    private static String TAG = "user.reflect";

    private String username = "hello";
    private Integer age;
    private String nickname;

    public User() {
    }

    public User(String username, Integer age, String nickname) {
        this.username = username;
        this.age = age;
        this.nickname = nickname;
    }

    private void showUserInfo() {
        System.out.printf("username: %s age: %d nickname: %s%n", username, age, nickname);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", age=" + age +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
