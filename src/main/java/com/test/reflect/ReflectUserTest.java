package com.test.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectUserTest {

    private static void reflectNewInstance() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> userClazz = Class.forName("com.test.reflect.User");
        User user = (User) userClazz.newInstance();
        user.setUsername("perter");
        user.setAge(20);
        user.setNickname("guys");
        System.out.println(user.toString());
    }

    private static void reflectPrivateConstructor() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Class<?> userClazz = Class.forName("com.test.reflect.User");

        Constructor<?> userConstructor = userClazz.getDeclaredConstructor(String.class, Integer.class, String.class);
        userConstructor.setAccessible(true);
        User user = (User) userConstructor.newInstance("bobby", 18, "rabbit");
        System.out.println("Constructor: " + user.toString());
    }

    private static void reflectPrivateField() throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        Class<?> userClazz = Class.forName("com.test.reflect.User");
        Object userObject = userClazz.newInstance();

        Field tagField = userClazz.getDeclaredField("TAG");
        tagField.setAccessible(true);
        String tagString = (String) tagField.get(userObject);
        System.out.println("tagString: " + tagString);

        Field usernameField = userClazz.getDeclaredField("username");
        usernameField.setAccessible(true);
        String usernameString = (String) usernameField.get(userObject);
        System.out.println("usernameString: " + usernameString);

        Field[] fields = userClazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.printf("member name: %s\n", field.getName());
            String result = (String) field.get(userObject);
            System.out.printf("number value: %s\n", result);
        }
    }

    private static void reflectPrivateMethod() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Class<?> userClazz = Class.forName("com.test.reflect.User");
        Object userObject = userClazz.newInstance();

        Method setUsernameMethod = userClazz.getDeclaredMethod("setUsername", String.class);
        Method getUsernameMethod = userClazz.getDeclaredMethod("getUsername");
        Method showUserInfoMethod = userClazz.getDeclaredMethod("showUserInfo");

        setUsernameMethod.invoke(userObject, "adam");
        String username = (String) getUsernameMethod.invoke(userObject);
        System.out.printf("username: %s\n", username);

        showUserInfoMethod.setAccessible(true);
        showUserInfoMethod.invoke(userObject);

        Method[] methods = userClazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.printf("method name: %s\n", method.getName());
        }
    }

    public static void main(String[] args) {
        try {
            reflectNewInstance();
            reflectPrivateConstructor();
            reflectPrivateField();
            reflectPrivateMethod();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
