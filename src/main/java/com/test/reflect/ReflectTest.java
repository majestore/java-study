package com.test.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectTest {

    public static void main(String[] args) {
        try {
            IOrder order = new SaleOrder();
            Class<?> orderClass = order.getClass();
//            Method setOrderMethod = orderClass.getDeclaredMethod("setOrder", String.class);
            Method setOrderDetailMethod = orderClass.getDeclaredMethod("setOrderDetail", String.class, int.class);
            Method getOrderMethod = orderClass.getDeclaredMethod("getOrder");
            Method showOrderMethod = orderClass.getDeclaredMethod("showOrder");
//            setOrderMethod.invoke(order, "hello world");
            setOrderDetailMethod.invoke(order, "superman", 9999);
            String result = (String) getOrderMethod.invoke(order);
            System.out.println("result: " + result);
            showOrderMethod.invoke(order);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
