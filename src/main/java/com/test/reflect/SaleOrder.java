package com.test.reflect;

public class SaleOrder implements IOrder {

    private String name;
    private int quantity;

    @Override
    public void setOrder(String name) {
        this.name = name;
    }

    @Override
    public void setOrderDetail(String name, int quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    @Override
    public String getOrder() {
        return this.name;
    }

    @Override
    public int getQuantity() {
        return this.quantity;
    }

    @Override
    public void showOrder() {
        System.out.printf("name: %s quantity: %d\n", this.name, this.quantity);
    }
}
