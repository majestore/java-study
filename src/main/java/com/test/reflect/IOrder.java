package com.test.reflect;

public interface IOrder {

    void setOrder(String name);

    void setOrderDetail(String name, int quantity);

    String getOrder();

    int getQuantity();

    void showOrder();
}
