package com.test.aop;

public interface UserService {
    void create();
    void delete();
    void update();
    void select();
}
