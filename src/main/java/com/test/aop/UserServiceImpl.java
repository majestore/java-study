package com.test.aop;

public class UserServiceImpl implements UserService {
    @Override
    public void create() {
        System.out.println("创建了一个用户");
    }

    @Override
    public void delete() {
        System.out.println("删除一个用户");
    }

    @Override
    public void update() {
        System.out.println("删除一个用户");
    }

    @Override
    public void select() {
        System.out.println("查询一个用户");
    }
}
