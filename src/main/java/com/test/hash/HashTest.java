package com.test.hash;

public class HashTest {

    public static void main(String[] args) {
        String str1 = "ab";
        long str1HashCode = str1.hashCode();
        System.out.println(str1HashCode);

        char[] str1Array = str1.toCharArray();
        System.out.println(Character.hashCode(str1Array[0]));
        System.out.println(Character.hashCode(str1Array[1]));
    }
}
