package com.test.adapter.exam1;

public class VideoInputAdapter extends Projector implements VideoInput {

    @Override
    public void outputSignal(String showContent) {
        super.outputSignal(showContent);
    }

    @Override
    public String vgaInput() {
        String vgaInput = super.vgaInput();
        return "adapter " + vgaInput;
    }
}
