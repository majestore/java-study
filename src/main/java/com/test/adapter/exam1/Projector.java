package com.test.adapter.exam1;

public class Projector implements VideoInput {

    private String outputSignal;

    public void outputSignal(String showContent) {
        this.outputSignal = showContent;
    }

    @Override
    public String vgaInput() {
        return this.outputSignal;
    }
}
