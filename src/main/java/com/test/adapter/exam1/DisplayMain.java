package com.test.adapter.exam1;

public class DisplayMain {

    public static void main(String[] args) {
        Projector projector = new Projector();
        projector.outputSignal("work report");
        ScreenDisplay screenDisplay = new ScreenDisplay();
        screenDisplay.receiveSignal(projector);

        VideoInputAdapter videoInputAdapter = new VideoInputAdapter();
        videoInputAdapter.outputSignal("play report");
        screenDisplay.receiveSignal(videoInputAdapter);
    }
}
