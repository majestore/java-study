package com.test.adapter.exam1;

public class ScreenDisplay {

    public void receiveSignal(VideoInput videoInput) {
        String inputContent = videoInput.vgaInput();
        System.out.printf("receive signal: %s\n", inputContent);
    }
}
