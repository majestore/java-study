package com.test.lambda;

public class LambdaSuper {
    static int staticFunc(int a, int b) {
        return a + b;
    }

    private int x = 0;
    private int y = 0;

    public LambdaSuper() {
    }

    public LambdaSuper(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int calc(int a, int b) {
        return a + b;
    }

    public static int getLambdaX(LambdaSuper lambdaSuper) {
        return lambdaSuper.getX();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
