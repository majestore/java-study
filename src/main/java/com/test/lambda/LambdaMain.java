package com.test.lambda;

import java.util.ArrayList;
import java.util.List;

public class LambdaMain {

    interface MathOperation {
        int operation(int a, int b);
    }

    public static void main(String[] args) {
        MathOperation mathOperation = (int a, int b) -> (a * b);

        Runnable runnable = () -> System.out.println("start thread");
        new Thread(runnable).start();

        new Thread(() -> System.out.println("other thread")).start();

        int result = mathOperation.operation(1, 2);
        System.out.println(result);

        Calculation calculation = (a, b, c) -> (a + b) * c;
        int calcResult = calculation.calculate(1, 2, 3);
        System.out.println(calcResult);

        int[] dataArray = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int dataInt : dataArray) {
            System.out.println(dataInt);
        }

        List<Integer> dataList = new ArrayList<Integer>() {
            {
                this.add(1);
                this.add(11);
                this.add(111);
                this.add(1111);
            }
        };
        dataList.forEach(item -> System.out.println("item: " + item));
    }
}
