package com.test.lambda;

public interface Calculation {
    int calculate(int a, int b, int c);
}
