package com.test.lambda;

public abstract class AbstractLambda {
    public abstract int operation(int a, int b);
}
