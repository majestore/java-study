package com.test.lambda;

public class LambdaTest {

    interface LambdaItf1 {
        int calc(int a, int b);
    }

    interface LambdaItf2 {
        LambdaSuper get(int x, int y);
    }

    interface LambdaItf3 {
        int getLambdaX(LambdaSuper lambdaSuper);
    }

    public static void main(String[] args) {
        LambdaItf1 lambdaItf1 = LambdaSuper::staticFunc;
        System.out.println(lambdaItf1.calc(1, 2));

        LambdaItf1 lambdaItf11 = new LambdaSuper()::calc;
        System.out.println(lambdaItf11.calc(2, 3));

        LambdaItf2 lambdaItf2 = LambdaSuper::new;
        LambdaSuper lambdaSuper = lambdaItf2.get(4, 5);
        System.out.println(lambdaSuper.getX());
        System.out.println(lambdaSuper.getY());

        LambdaItf3 lambdaItf3 = LambdaSuper::getLambdaX;
        int getX = lambdaItf3.getLambdaX(lambdaSuper);
        System.out.println(getX);
    }
}
