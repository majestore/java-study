package com.test.fanxing;

public class FanXingTest {

    public static void main(String[] args) {
        FanClass<Integer> fan1 = new FanClass<>();
        fan1.setMember(1);
        System.out.println(fan1.getMember());

        FanClass<String> fan2 = new FanClass<>();
        fan2.setMember("hello");
        System.out.println(fan2.getMember());

        FanMethod fanMethod = new FanMethod();
        fanMethod.showInfo(111);
        fanMethod.showInfo("world");
        fanMethod.showInfo2(123);
        fanMethod.showInfo3(Integer.class);
    }
}
