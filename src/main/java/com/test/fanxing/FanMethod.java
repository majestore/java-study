package com.test.fanxing;

public class FanMethod {
    public <FA> void showInfo(FA data) {
        System.out.println(data);
    }

    public <FB extends Integer> void showInfo2(FB data) {
        System.out.println(data);
    }

    public void showInfo3(Class<? extends Integer> clazz) {
        System.out.println(clazz);
    }
}
