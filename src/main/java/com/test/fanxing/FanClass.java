package com.test.fanxing;

public class FanClass<T> {
    private T member;

    public T getMember() {
        return member;
    }

    public void setMember(T member) {
        this.member = member;
    }
}
