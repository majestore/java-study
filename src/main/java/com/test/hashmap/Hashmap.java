package com.test.hashmap;

import java.util.*;

public class Hashmap {
    public static void main(String[] args) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("name", "peter");
        mapData.put("age", 20);
        mapData.put("name1", "josan1");
        mapData.put("name2", "josan2");
        mapData.put("name3", "josan3");
        System.out.println(mapData.toString());
        System.out.println();

        Set<Map.Entry<String, Object>> entrySet = mapData.entrySet();
        for (Map.Entry<String, Object> entry : entrySet) {
            System.out.println(entry.toString());
        }

        Map<String, Object> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("name", "jack");
        linkedHashMap.put("birthday", new Date());
        linkedHashMap.put("phone", 1001);
        linkedHashMap.put("name1", "josan1");
        linkedHashMap.put("name2", "josan2");
        linkedHashMap.put("name3", "josan3");
        System.out.println(linkedHashMap.toString());
        System.out.println();

        TreeMap<String, Object> treeMap = new TreeMap<>((Object o1, Object o2) -> {
            String i1 = (String) o1;
            String i2 = (String) o2;
            int result = i1.compareTo(i2);
            System.out.println("result:" + result + " i1:" + i1 + " i2:" + i2);
            return -result;
        });
        treeMap.put("key_1", 1);
        treeMap.put("key_2", 2);
        treeMap.put("key_5", 3);
        System.out.println(treeMap.toString());
        System.out.println();
    }
}
