package com.test.hashmap;

public class CusHashmap {
    public static void main(String[] args) {
        //hash map
        QHashMap<String, String> map = new QHashMap<>();
        map.put("name", "tom");
        map.put("age", "23");
        map.put("address", "beijing");
        String oldValue = map.put("address", "shanghai"); //key一样，返回旧值，保存新值

        System.out.println(map.get("name"));
        System.out.println(map.get("age"));

        System.out.println("旧值=" + oldValue);
        System.out.println("新值=" + map.get("address"));

        //linked hash map
        QLinkedHashMap<String,String> linkedHashMap = new QLinkedHashMap<>();
        map.put("name","tom");
        map.put("age","32");
        map.put("address","beijing");

        //验证是不是按照插入的顺序打印
        QLinkedHashMap.QIterator iterator =  linkedHashMap.iterator();
        while (iterator.hasNext()){
            QLinkedHashMap.QEntry e = iterator.next();
            System.out.println("key=" + e.key + " value=" + e.value);
        }
    }
}
